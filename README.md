Hey, I'm theee new Shweta! 👋
passionate about creating all-things-tech to improve society.

<!--START_SECTION:random_quote-->
# Do read a quote before you go 

The more sand that has escaped from the hourglass of our life, the clearer we should see through it.
<!--END_SECTION:random_quote-->


<!--START_SECTION:blog_post-->
- [Introducing ToolHub](https://shwetakale.hashnode.dev/introducing-toolhub)
- [Krishi](https://shwetakale.hashnode.dev/krishi)
- [Pixy - Create Pixel Art](https://shwetakale.hashnode.dev/pixy-create-pixel-art)
- [Communities that help me flourish](https://shwetakale.hashnode.dev/communities-that-help-me-flourish)
- [From Solving to Creating...](https://shwetakale.hashnode.dev/from-solving-to-creating)
- [January Wrap: Personal Retrospective](https://shwetakale.hashnode.dev/january-wrap-personal-retrospective)
- [2023: A Fresh Start - Reflecting on the First Two Weeks of the New Year](https://shwetakale.hashnode.dev/2023-a-fresh-start-reflecting-on-the-first-two-weeks-of-the-new-year)
- [Veron - Ecommerce using Carbon Aware API](https://shwetakale.hashnode.dev/veron-ecommerce-using-carbon-aware-api)
- [My first development job at a startup: My story](https://shwetakale.hashnode.dev/my-first-development-job-at-a-startup-my-story)
- [clearThoughts -  Express yourself more clearly, completely, and effectively](https://shwetakale.hashnode.dev/clearthoughts-express-yourself-more-clearly-completely-and-effectively)

<!--END_SECTION:blog_post-->